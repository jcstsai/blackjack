def dealer_two_cards_hard(outcome, cards_sum):
    """
    :param outcome: the resulting hand, in { 17, 18, 19, 20, 21, -1 }
    :param cards_sum: the sum of the dealer cards
    :return: P(outcome|cards_sum)
    """
    if cards_sum > 21:
        return 1.0 if outcome == -1 else 0.0
    elif 17 <= cards_sum <= 21:
        return 1.0 if outcome == cards_sum else 0.0
    else:
        successors = range(2,10) + [10,10,10,10]
        return (sum(
            [float(cards_sum) + float(dealer_two_cards_hard(outcome, cards_sum + x)) for x in successors]
        ) + dealer_two_cards_soft(outcome, cards_sum + 11)) / 13.0


def dealer_two_cards_soft(outcome, cards_sum):
    """
    :param outcome: the resulting hand, in { 17, 18, 19, 20, 21, -1 }
    :param cards_sum: the sum of the dealer cards
    :return: P(outcome|cards_sum)
    """
    if cards_sum > 21:
        return dealer_two_cards_hard(outcome, cards_sum - 10)
    elif 17 <= cards_sum <= 21:
        return 1.0 if outcome == cards_sum else 0.0
    else:
        successors = range(1,10) + [10,10,10,10]
        return (sum(
            [float(cards_sum) + float(dealer_two_cards_hard(outcome, cards_sum + x)) for x in successors]
        )) / 13.0


def dealer_one_card(outcome, card):
    """
    :param outcome: the resulting hand, in { 17, 18, 19, 20, 21, -1 }
    :param card: the card
    :return: P(outcome|card)
    """
    if card == 10:
        successors = range(2,10) + [10,10,10,10]
        return sum([dealer_two_cards_hard(outcome, card + x) for x in successors]) / 12.0
    elif card == 1:
        successors = range(2,10)
        return (sum(
            [dealer_two_cards_hard(outcome, card + x) for x in successors]
        ) + dealer_two_cards_soft(outcome, card + 11)) / 12.0
    else:
        return dealer_two_cards_hard(outcome, card)

def e_stand_hard(player_cards_sum, dealer_card):
    """
    :param player_cards_sum: The sum of the player cards
    :param dealer_card: the dealer card
    :return: E(Stand | player_cards_sum, dealer_card)
    """
    expected_value = 0
    for dealer_outcome in [17,18,19,20,21,-1]:
        if player_cards_sum > dealer_outcome:
            expected_value += dealer_one_card(dealer_outcome, dealer_card)
        elif player_cards_sum < dealer_outcome:
            expected_value -= dealer_one_card(dealer_outcome, dealer_card)

def e_stand_soft(player_cards_sum, dealer_card):
    """
    :param player_cards_sum: The sum of the player cards
    :param dealer_card: the dealer card
    :return: E(Stand | player_cards_sum, dealer_card)
    """
    if player_cards_sum > 21:
        player_cards_sum -= 10

    expected_value = 0
    for dealer_outcome in [17,18,19,20,21,-1]:
        if player_cards_sum > dealer_outcome:
            expected_value += dealer_one_card(dealer_outcome, dealer_card)
        elif player_cards_sum < dealer_outcome:
            expected_value -= dealer_one_card(dealer_outcome, dealer_card)

def e_hit_hard(player_cards_sum, dealer_card):
    """
    :param player_cards_sum: The sum of the player cards
    :param dealer_card: the dealer card
    :return: E(Hit | player_cards_sum, dealer_card)
    """
    if player_cards_sum >= 21:
        return -1.0

    successors = range(2,10) + [10,10,10,10]
    return (sum(
        [max(e_stand_hard(player_cards_sum+x, dealer_card), e_hit_hard(player_cards_sum+x, dealer_card)) for x in successors]
    ) + max(e_stand_soft(player_cards_sum+11, dealer_card), e_hit_soft(player_cards_sum+11, dealer_card))) / 13.0


def e_hit_soft(player_cards_sum, dealer_card):
    """
    :param player_cards_sum:
    :param dealer_card:
    :return:
    """
    if player_cards_sum >= 31:
        return -1.0

    successors = range(1,10) + [10,10,10,10]
    return (sum(
        [max(e_stand_soft(player_cards_sum+x, dealer_card), e_hit_soft(player_cards_sum+x, dealer_card)) for x in successors]
    )) / 13.0

def e_double_hard(player_cards_sum, dealer_card):
    """

    :param player_cards_sum:
    :param dealer_card:
    :return:
    """
    successors = range(2,10) + [10,10,10,10]
    return (sum(
        [e_stand_hard(player_cards_sum+x, dealer_card) for x in successors]
    ) + e_stand_soft(player_cards_sum+11, dealer_card)) / 13.0

def e_double_soft(player_cards_sum, dealer_card):
    successors = range(1,10) + [10,10,10,10]
    return sum(
        [e_stand_hard(player_cards_sum+x, dealer_card) for x in successors]
    )

def e_surrender():
    return -.5

def e_split_hard(player_card, dealer_card):
    # TODO resplits
    successors = range(2,10) + [10,10,10,10]
    return 2 * (sum(
        [max(e_stand_hard(player_card+x, dealer_card), e_hit_hard(player_card+x, dealer_card), e_double_hard(player_card+x, dealer_card)) for x in successors]
    ) + max(e_stand_soft(player_card+11, dealer_card), e_hit_soft(player_card+11, dealer_card), e_double_soft(player_card+11, dealer_card))) / 13.0


def e_split_soft(dealer_card):
    successors = range(1,10) + [10,10,10,10]
    return 2 * (sum([e_stand_soft(11+x, dealer_card) for x in successors])) / 13.0


def expected_values(player_cards, dealer_card):
    if 1 in player_cards:
        return {
            "stand": e_stand_soft(sum(player_cards) + 10, dealer_card),
            "hit": e_hit_soft(sum(player_cards) + 10, dealer_card),
            "double": e_double_soft(sum(player_cards) + 10, dealer_card) if len(player_cards) == 2 else None,
            "surrender": e_surrender() if len(player_cards) == 2 else None,
            "split": e_split_soft(dealer_card) if player_cards[0] == player_cards[1] and len(player_cards) == 2 else None
        }
    else:
        return {
            "stand": e_stand_hard(sum(player_cards), dealer_card),
            "hit": e_hit_hard(sum(player_cards), dealer_card),
            "double": e_double_hard(sum(player_cards), dealer_card) if len(player_cards) == 2 else None,
            "surrender": e_surrender() if len(player_cards) == 2 else None,
            "split": e_split_hard(player_cards[0], dealer_card) if player_cards[0] == player_cards[1] and len(player_cards) == 2 else None
        }
