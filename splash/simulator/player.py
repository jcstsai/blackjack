from enums import Move

class Player(object):
    baseline_bet = 25.0

    def __init__(self, baseline_bet):
        self.baseline_bet = baseline_bet

    def bet(self, count):
        if 1.0 <= count < 2.0:
            return self.baseline_bet * 2.0
        elif 2.0 <= count < 3.0:
            return self.baseline_bet * 4.0
        elif 3.0 <= count < 4.0:
            return self.baseline_bet * 6.0
        elif 4.0 <= count:
            return self.baseline_bet * 8.0
        else:
            return self.baseline_bet

    def move(self, cards, dealer_card, can_surrender, can_double, can_split):
        # Check bust
        if sum(cards) > 21:
            return Move.Stand

        # check for split
        if can_split and cards[0] == cards[1]:
            # Split AA vs *
            if cards[0] == 1:
                return Move.Split
            # Split 99 vs 2-6, 8-9
            elif cards[0] == 9 and dealer_card in (range(2,7) + [8,9]):
                return Move.Split
            # Split 88 vs 2-10
            elif cards[0] == 8 and dealer_card != 1:
                return Move.Split
            # Split 22,33,77 vs 2-7
            elif cards[0] in (2,3,7) and dealer_card in range(2,8):
                return Move.Split
            # Split 66 vs 2-6
            elif cards[0] == 6 and dealer_card in range(2,7):
                return Move.Split
            # Split 44 vs 5-6
            elif cards[0] == 4 and dealer_card in [5,6]:
                return Move.Split

        # check for surrender (hard hands only)
        if can_surrender:
            # surrender 17 vs A
            if sum(cards) == 17 and dealer_card == 1:
                return Move.Surrender
            # surrender 16 vs 9, 10, or A
            elif sum(cards) == 16 and dealer_card in (9,10,1):
                return Move.Surrender
            # surrender 15 vs 10 or A
            elif sum(cards) == 15 and dealer_card in (10,1):
                return Move.Surrender

        # check for soft hands
        if sum(cards) < 12 and 1 in cards:
            soft_sum = sum(cards) + 10

            if can_double:
                # Double 19 vs 6
                if soft_sum == 19 and dealer_card == 6:
                    return Move.Double
                # Double 18 vs 2-6
                elif soft_sum == 18 and dealer_card in range(2,7):
                    return Move.Double
                # Double 17 vs 3-6
                elif soft_sum == 17 and dealer_card in range(3,7):
                    return Move.Double
                # Double 15-16 vs 4-6
                elif soft_sum in (15, 16) and dealer_card in (4,5,6):
                    return Move.Double
                # Double 13-14 vs 5-6
                elif soft_sum in (13, 14) and dealer_card in (5,6):
                    return Move.Double

            # Stand 19-21 vs *
            if soft_sum > 18:
                return Move.Stand
            # Stand 18 vs 2-8
            elif soft_sum == 18 and dealer_card not in (9,10,1):
                return Move.Stand
            # Hit <= 17 vs * and 18 vs 9, 10, A
            else:
                return Move.Hit

        # Check for doubling
        if len(cards) == 2:
            # Double 11 vs *
            if sum(cards) == 11:
                return Move.Double
            # Double 10 vs 2-9
            elif sum(cards) == 10 and dealer_card in range(2, 10):
                return Move.Double
            # Double 9 vs 3-6
            elif sum(cards) == 9 and dealer_card in range(3, 7):
                return Move.Double

        # Stand 17-21 vs *
        if sum(cards) >= 17:
            return Move.Stand
        # Stand 13-16 vs 2-6
        elif sum(cards) > 12 and dealer_card != 1 and dealer_card <= 6:
            return Move.Stand
        # Stand 12 vs 4-6
        elif sum(cards) == 12 and dealer_card in (4, 5, 6):
            return Move.Stand
        # Hit 11- vs *, 12 vs 2-3, 12+ vs 7+
        else:
            return Move.Hit
