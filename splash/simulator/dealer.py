from enums import Move


class Dealer(object):
    hit_17 = None

    def __init__(self, hit_17=True):
        self.hit_17 = hit_17

    def move(self, cards):
        # Check bust
        if sum(cards) > 21:
            return Move.Stand

        # soft hands
        if sum(cards) < 12 and 1 in cards:
            soft_sum = sum(cards) + 10
            if soft_sum > (17 if self.hit_17 else 16):
                return Move.Stand
            else:
                return Move.Hit

        # hard hands
        if sum(cards) >= 17:
            return Move.Stand
        else:
            return Move.Hit
