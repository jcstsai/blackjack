"""
This program allows you to simulate various card-counting strategies and move patterns on a large number of trials.
Usage: python blackjack.py
"""

from dealer import Dealer
from deck import Deck
from enums import Move
from player import Player

# user-defined constants
NUM_ITERATIONS = 10000
NUM_DECKS = 6
BASELINE_BET = 25.0
HIT_17 = True
# TODO more constants can be added here


class Blackjack(object):
    def __init__(self):
        pass

    def run(self):
        average_money = 0.0

        for i in xrange(NUM_ITERATIONS):
            deck = Deck(num_decks=NUM_DECKS)
            player = Player(baseline_bet=BASELINE_BET)
            dealer = Dealer(hit_17=HIT_17)
            money = 0.0

            while True:
                # 1) Player makes bet
                bet = player.bet(deck.get_count())

                # 2) Cards are drawn
                player_cards = [deck.draw(), deck.draw()]
                dealer_cards = [deck.draw(), deck.draw()]

                # 3) Check for blackjack end condition
                player_blackjack = player_cards in ([10,1], [1,10])
                dealer_blackjack = dealer_cards in ([10,1], [1,10])
                if player_blackjack and not dealer_blackjack:
                    money += bet * 1.5
                elif dealer_blackjack and not player_blackjack:
                    money -= bet
                if (player_blackjack or dealer_blackjack) and deck.get_num_decks() < (float(NUM_DECKS) / 3.0):
                    break
                elif player_blackjack or dealer_blackjack:
                    continue

                # 4) player move
                # TODO implement splits
                can_surrender = True
                can_double = True
                can_split = False
                surrendered = False
                while True:
                    player_move = player.move(player_cards, dealer_cards[0], can_surrender, can_double, can_split)
                    can_surrender = False
                    can_double = False
                    can_split = False
                    if player_move == Move.Stand:
                        break
                    elif player_move == Move.Double:
                        player_cards.append(deck.draw())
                        break
                    elif player_move == Move.Surrender:
                        surrendered = True
                        break
                    elif player_move == Move.Hit:
                        player_cards.append(deck.draw())
                    else:
                        raise Exception("Invalid player move " + str(player_move))

                # 5) Check for surrender end condition
                if surrendered and deck.get_num_decks() < (float(NUM_DECKS) / 3.0):
                    money -= bet * 0.5
                    break
                elif surrendered:
                    money -= bet * 0.5
                    continue

                # 6) dealer move
                while True:
                    dealer_move = dealer.move(dealer_cards)
                    if dealer_move == Move.Stand:
                        break
                    elif dealer_move == Move.Hit:
                        dealer_cards.append(deck.draw())
                    else:
                        raise Exception("Invalid dealer move " + str(dealer_move))

                # 7) resolve winner
                if 1 in player_cards and sum(player_cards) <= 11:
                    player_cards[player_cards.index(1)] = 11
                if 1 in dealer_cards and sum(dealer_cards) <= 11:
                    dealer_cards[dealer_cards.index(1)] = 11
                if sum(player_cards) > sum(dealer_cards):
                    money += bet
                elif sum(player_cards) < sum(dealer_cards):
                    money -= bet
                else:
                    pass

                # 8) check for end condition
                if deck.get_num_decks() < (float(NUM_DECKS) / 3.0):
                    break

            average_money = (average_money * float(i) + money) / (i+1)
            print("Trial " + str(i) + ": " + str(money))

        print ("Average money: " + str(average_money))

Blackjack().run()
