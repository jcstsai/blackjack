import random


class Deck(object):
    cards = None
    num_decks = None
    discard_pile = None
    count = None

    def __init__(self, num_decks=6):
        self.cards = (range(1,11) + [10,10,10]) * (num_decks * 4)
        self.num_decks = num_decks
        self.count = 0
        random.shuffle(self.cards)

    def draw(self):
        if len(self.cards) == 0:
            return None

        next_card = self.cards[0]
        self.cards = self.cards[1:]
        if next_card == 10 or next_card == 1:
            self.count -= 1
        elif next_card in (2,3,4,5,6):
            self.count += 1
        return next_card

    def get_num_decks(self):
        return float(len(self.cards)) / 52.0

    def get_count(self):
        return float(self.count) / self.get_num_decks()