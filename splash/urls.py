from django.conf.urls import url

import splash
import views

urlpatterns = [
    url(r'^$', views.index)
]